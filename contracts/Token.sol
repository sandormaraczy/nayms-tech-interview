pragma solidity 0.7.0;

import "./IERC20.sol";
import "./IMintableToken.sol";
import "./IDividends.sol";
import "./SafeMath.sol";

contract Token is IERC20, IMintableToken, IDividends {
  // ------------------------------------------ //
  // ----- BEGIN: DO NOT EDIT THIS SECTION ---- //
  // ------------------------------------------ //
  using SafeMath for uint256;
  uint256 public totalSupply;
  uint256 public decimals = 18;
  string public name = "Test token";
  string public symbol = "TEST";
  address[] public holders;
  mapping (address => uint256) public balanceOf;
  mapping (address => uint256) private dividendOf;
  mapping(address => mapping(address => uint256)) private allowances;

  // ------------------------------------------ //
  // ----- END: DO NOT EDIT THIS SECTION ------ //  
  // ------------------------------------------ //

  // IERC20

  function allowance(address owner, address spender) external view override returns (uint256) {
    return allowances[owner][spender];
  }

  function transfer(address to, uint256 value) external override returns (bool) {
    return transfer(msg.sender, to, value);
  }

  function transfer(address from, address to, uint256 value) private returns (bool) {
    uint256 senderBal = balanceOf[from];
    require(senderBal >= value, "transfer amount exceeds TEST token balance");
    balanceOf[from] = senderBal - value;
    balanceOf[to] += value;
    if (!doesHolderExist(to)) {
      holders.push(to);
    }

    return true;
  }

  function doesHolderExist(address holderAddress) public view returns (bool) {
    for(uint i = 0; i < holders.length; i++) {
      if (holders[i] == holderAddress) {
        return true;
      }
    }
    return false;
  }

  function approve(address spender, uint256 value) external override returns (bool) {
    allowances[msg.sender][spender] = value;
    return true;
  }

  function transferFrom(address from, address to, uint256 value) external override returns (bool) {
    uint256 allowance = allowances[from][msg.sender];
    require(allowance >= value, "transfer amount exceeds allowance");
    allowances[from][msg.sender] -= value;
    transfer(from, to, value);

    return true;
  }

  // IMintableToken

  function mint() external payable override {
    require(msg.value > 0);
    balanceOf[msg.sender] += msg.value;
    totalSupply += msg.value;
    if (!doesHolderExist(msg.sender)) {
      holders.push(msg.sender);
    }
  }

  function burn(address payable dest) external override {
    uint balance = balanceOf[msg.sender];
    if (balance > 0) {
      balanceOf[msg.sender] = 0;
      totalSupply -= balance;
      dest.transfer(balance);
    }
  }

  // IDividends
  
  function recordDividend() external payable override {
    require(msg.value > 0);

    for (uint i = 0; i < holders.length; i++) {
      address holderAddr = holders[i];
      if (balanceOf[holderAddr] > 0) {
        uint256 rate = divide(balanceOf[holderAddr], totalSupply, 4);
        dividendOf[holderAddr] += SafeMath.mul(msg.value, rate) / 10000;
      }
    }
  }

  function getWithdrawableDividend(address payee) external view override returns (uint256) {
    return dividendOf[payee];
  }

  function withdrawDividend(address payable dest) external override {
    uint dividendBal = dividendOf[msg.sender];
    if (dividendBal > 0) {
      dividendOf[msg.sender] = 0;
      dest.transfer(dividendBal);
    }
  }

  function divide(uint numerator, uint denominator, uint precision) private pure returns(uint) {
    uint raisedNumerator = numerator * 10 ** (precision + 1);
    return ((raisedNumerator / denominator) + 10) / 10;
  }
}